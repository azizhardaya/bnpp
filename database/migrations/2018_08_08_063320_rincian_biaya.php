<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RincianBiaya extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('rincian_biaya', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nip');
          $table->string('nama_pegawai');
          $table->string('harian');
          $table->string('transport');
          $table->string('uang_hotel');
          $table->string('jabatan');
          $table->string('kegiatan');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rincian_biaya');
    }
}
