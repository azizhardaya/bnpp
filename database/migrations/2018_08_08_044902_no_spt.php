<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NoSpt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('no_spt', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nip');
          $table->string('nama_pegawai');
          $table->string('no_spt');
          $table->string('golongan');
          $table->string('kegiatan');
          $table->string('rincian_biaya');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('no_spt');
    }
}
