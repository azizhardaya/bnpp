<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Datapegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('data_pegawai', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nip');
          $table->string('nama_pegawai');
          $table->string('alamat');
          $table->string('golongan');
          $table->string('jabatan');
          $table->string('no_tlp');
          $table->string('email');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pegawai');
    }
}
