<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LaporanSppd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('laporan_sppd', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nip');
          $table->string('nama_pegawai');
          $table->string('no_sppd');
          $table->string('kegiatan');
          $table->string('t_berangkat');
          $table->string('t_tujuan');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_sppd');
    }
}
