@extends('template.app')

{{-- set title --}}
@section('title', 'Manage Posting')

{{-- set main content --}}
@section('content')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>BNPP</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>BNPP</b></span>
    </a>

     <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
          @include('template.menu')
      </nav>
    </header>

         @include('template.sidebar')

    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content-header">
        <h1>
            No Surat
        </h1>
        <ol class="breadcrumb">
          <li>Menu</li>
          <li class="active">Tambah No Surat</li>
        </ol>
      </section>

      <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header with-border">
                            <h3 class="box-title"> Tambah No Surat</h3>
                              <div class="box-tools">
                              </div>
                </div>
               <div class="box-body table-responsive">
                 <form class="form-horizontal col-md-6" action="{{ url('/no_surat/do_create') }}" method="post">
                   {{-- set token --}}
                        {{ csrf_field() }}

                    <div class="form-group">
                      <label  class="col-sm-5 control-label">NIP :</label>
                      <div class="col-sm-7">
                        <select class="form-control col-md-7" name="nip" required>
                          <option value="">-- Select --</option>
                    @foreach($nip as $category)
                          <option value="{!! $category->nip !!}">{{ $category->nip }}</option>
                    @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label  class="col-sm-5 control-label">No. Surat :</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control col-md-7" name="no_surat" placeholder="Enter ..." required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label  class="col-sm-5 control-label">Isi Surat :</label>
                      <div class="col-sm-7">
                          <textarea class="form-control col-md-7" name="isi_surat" rows="8" cols="50" placeholder="Enter ..." required></textarea>
                      </div>
                    </div>

                   <div class="form-grup">
                     <input type="submit" class="btn btn-success pull-right" value="Submit">
                     <a href="{{ url('no_surat') }}" class="btn btn-default pull-right" style="margin-right: 20px;"> Cancel </a>
                   </div>
                </form>
              </div>
            </div>
          </div>
          </div>
      </section>

    </div>
  </div>

  <script>
      $(function () {
        $('#table-home tfoot th').each( function () {
          var title = $(this).text();
          $(this).html( '<input style="width:100%;" type="text" placeholder="Search '+title+'" />' );
        });

        var table = $('#table-home').DataTable({
          responsive: true,
          stateSave: true,
          "paging": true,
          "lengthChange": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
          "order": [[ 0, "desc" ]],
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          dom: 'lrtipB',
          buttons: [
                  'copy', 'csv', 'excel', 'pdf', 'print'
          ]
        });
        // Apply the search
        table.columns().every( function () {
          var that = this;
          $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
              that
              .search( this.value )
              .draw();
            }
          });
        });

        // for datetimepicker


      });
    </script>
<body>
@endsection
