@extends('template.app')

{{-- set title --}}
@section('title', 'Manage Posting')

{{-- set main content --}}
@section('content')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>BNPP</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>BNPP</b></span>
    </a>

     <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
          @include('template.menu')
      </nav>
    </header>

         @include('template.sidebar')

    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content-header">
        <h1>
              No Surat
        </h1>
        <ol class="breadcrumb">
          <li>Menu</li>
          <li class="active">No Surat</li>
        </ol>
      </section>
      <!-- Main content -->
         <section class="invoice">
           <!-- title row -->
           <div class="row">
             <div class="col-xs-12">
               <h2 class="page-header">
                 BNPP
                 <!-- <small class="pull-right">Date: 2/10/2014</small> -->
               </h2>
             </div>
             <!-- /.col -->
           </div>
           <!-- info row -->
           <div class="row invoice-info">
             <div class="col-sm-12 invoice-col" style="text-align: center;">
               <h1>No Surat</h1>
             </div>
             <!-- /.col -->
             <!-- <div class="col-sm-4 invoice-col">
               To
               <address>
                 <strong>John Doe</strong><br>
                 795 Folsom Ave, Suite 600<br>
                 San Francisco, CA 94107<br>
                 Phone: (555) 539-1037<br>
                 Email: john.doe@example.com
               </address>
             </div> -->
             <!-- /.col -->
             <!-- /.col -->
           </div>
           <!-- /.row -->
         </br>


           <div class="row">
             <!-- accepted payments column -->

             <!-- /.col -->
             <div class="col-xs-10">
               <div class="table-responsive">
                 <table class="table">
                   <tr>
                     <th style="width:50%">NIP :</th>
                     <td>{{$no_surat->nip}}</td>
                   </tr>
                   <tr>
                     <th>No Surat :</th>
                     <td>{{$no_surat->no_surat}}</td>
                   </tr>
                   <tr>
                     <th>Isi Surat :</th>
                     <td>{{$no_surat->isi_surat}}</td>
                   </tr>

                 </table>
               </div>
             </div>
           </div>
           <!-- /.row -->

           <!-- this row will not appear when printing -->
           <div class="row no-print">
             <div class="col-xs-12">
               <!-- <a href="invoice-print.html" target="_blank" class="btn btn-default pull-right"><i class="fa fa-print"></i> Print</a> -->
               <a href="{{ url('no_surat') }}" class="btn btn-default pull-left" style="margin-right: 20px;"> Back </a>
             </div>
           </div>
         </section>

    </div>
  </div>

  <script>
      $(function () {
        $('#table-home tfoot th').each( function () {
          var title = $(this).text();
          $(this).html( '<input style="width:100%;" type="text" placeholder="Search '+title+'" />' );
        });

        var table = $('#table-home').DataTable({
          responsive: true,
          stateSave: true,
          "paging": true,
          "lengthChange": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
          "order": [[ 0, "desc" ]],
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          dom: 'lrtipB',
          buttons: [
                  'copy', 'csv', 'excel', 'pdf', 'print'
          ]
        });
        // Apply the search
        table.columns().every( function () {
          var that = this;
          $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
              that
              .search( this.value )
              .draw();
            }
          });
        });

        // for datetimepicker


      });
    </script>
<body>
@endsection
