@extends('template.app')

{{-- set title --}}
@section('title', 'Manage Posting')

{{-- set main content --}}
@section('content')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>BNPP</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>BNPP</b> </span>
    </a>

     <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
          @include('template.menu')
      </nav>
    </header>

         @include('template.sidebar')

    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content-header">
        <h1>
              Laporan SPPD
        </h1>
        <ol class="breadcrumb">
          <li>Menu</li>
          <li class="active">Laporan SPPD</li>
        </ol>
      </section>

      <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <a href="{{ url('laporan_sppd/create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> &nbsp;Tambah Laporan SPPD</a>
                    <div class="box-tools">
                      <form class="form-group" action="{{ url('#') }}" method="get">
                        <div class="input-group input-group-sm" style="width: 250px;">
                            <input type="text" name="search" class="form-control pull-right" placeholder="Search">
                              <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                              </div>
                        </div>
                      </form>
                    </div>
                </div>

        <div class="box-body table-responsive">
        <table id="table-home" class="table table-bordered table-hover" border="10">
        <!-- <thead> -->
          <tr style="background-color: #eaecef;">
            <th width="5%">No</th>
            <th>NIP</th>
            <th>Nama Pegawai</th>
            <th>No. SPPD</th>
            <th width="25%">Action</th>
          </tr>
        @foreach($laporan_sppd as $k => $items)
          <tr>
            <td>{{ ($laporan_sppd->currentpage()-1) * $laporan_sppd->perpage() + $k + 1 }}</td>
            <td>{{ $items->nip }}</td>
            <td>{{ $items->nama_pegawai }}</td>
            <td>{{ $items->no_sppd }}</td>
            <td><a href="{{ url('/laporan_sppd/view/'.$items->id) }}" class="btn btn-info js-scroll-trigger"><i class="fa fa-eye"></i> View</a> <a href="{{ url('/laporan_sppd/edit/'.$items->id) }}" class="btn btn-warning js-scroll-trigger"><i class="fa fa-edit"></i> Edit</a> <a href="{{ url('/laporan_sppd/delete/'.$items->id) }}" class="btn btn-danger js-scroll-trigger" onclick="return confirm('Are you sure you want to delete this data ?')"><i class="fa fa-trash"></i> Delete</a></td>
          </tr>
        @endforeach
        </table>
        <div class="box-footer clearfix">
          <ul class="pagination pagination-sm no-margin pull-right">
              <li>{{ $laporan_sppd->links() }}</li>
          </ul>
        </div>
        </div>
          </div>
          </div>
      </section>

    </div>
  </div>

  <script>
      $(function () {
        $('#table-home tfoot th').each( function () {
          var title = $(this).text();
          $(this).html( '<input style="width:100%;" type="text" placeholder="Search '+title+'" />' );
        });

        var table = $('#table-home').DataTable({
          responsive: true,
          stateSave: true,
          "paging": true,
          "lengthChange": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
          "order": [[ 0, "desc" ]],
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          dom: 'lrtipB',
          buttons: [
                  'copy', 'csv', 'excel', 'pdf', 'print'
          ]
        });
        // Apply the search
        table.columns().every( function () {
          var that = this;
          $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
              that
              .search( this.value )
              .draw();
            }
          });
        });

        // for datetimepicker


      });
    </script>
<body>
@endsection
