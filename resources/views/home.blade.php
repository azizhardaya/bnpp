@extends('template.app')

{{-- set title --}}
@section('title', 'Manage Posting')

{{-- set main content --}}
@section('content')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>BNPP</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>BNPP</b></span>
    </a>

     <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
          @include('template.menu')
      </nav>
    </header>

         @include('template.sidebar')

    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content-header">
        <h1>
          Home
        </h1>
        <ol class="breadcrumb">
          <li>Menu</li>
          <li class="active">Home</li>
        </ol>
      </section>

      <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="row">
                   <div class="col-md-12">
                     <!-- Custom Tabs -->
                     <div class="nav-tabs-custom">
                       <ul class="nav nav-tabs">
                         <li class="active"><a href="#tab_1" data-toggle="tab"><b>Sejarah</b></a></li>
                         <li><a href="#tab_2" data-toggle="tab"><b>Visi dan Misi</b></a></li>
                         <li><a href="#tab_3" data-toggle="tab"><b>Alamat</b></a></li>

                         <!-- <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li> -->
                       </ul>
                       <div class="tab-content">
                         <div class="tab-pane active" id="tab_1">
                           <p>Pengelolaan perbatasan merupakan bagian integral dari manajemen negara, yang secara operasional merupakan kegiatan penanganan atau mengelola batas wilayah dan kawasan perbatasan. Sejalan dengan reorientasi kebijakan pembangunan di kawasan perbatasan, melalui Undang-Undang Nomor 43 Tahun 2008 tentang Wilayah Negara yang memberikan mandat kepada Pemerintah untuk membentuk Badan Pengelola Perbatasan di tingkat pusat dan daerah dalam rangka mengelola kawasan perbatasan. Berdasarkan amanat UU tersebut, Pemerintah melalui Peraturan Presiden Nomor 12 Tahun 2010 membentuk Badan Nasional Pengelola perbatasan (BNPP). Dalam konteks pengelolaan batas wilayah negara dan kawasan perbatasan, BNPP mengedepankan sinergi kebijakan dan program, sehingga kelemahan dan keterbatasan yang ada selama ini, yakni penanganan perbatasan negara secara ad-hoc dan parsial serta egosektoral, yang telah mengakibatkan overlapping dan redundance serta salah sasaran dan inefisiensi dalam pengelolaan perbatasan, diharapkan dapat diperbaiki. Ruang lingkup tugas utama BNPP adalah mengelola Batas Wilayah Negara dan meningkatkan kesejahteraan masyarakat di kawasan perbatasan yang merupakan kristalisasi dari amanat Undang-Undang Nomor 43 Tahun 2008 pasal 15 dan Peraturan Presiden Nomor 12 Tahun 2010 pasal 3, sebagai berikut :</p>
                           <ul>
                               <li>Menetapkan kebijakan program pembangunan perbatasan</li>
                               <li>Menetapkan rencana kebutuhan anggaran</li>
                               <li>Mengkoordinasikan pelaksanaan dan</li>
                               <li>Melaksanakan evaluasi dan pengawasan terhadap pengelolaan Batas Wilayah Negara dan Kawasan Perbatasan.</li>
                          </ul>

                         </div>
                         <!-- /.tab-pane -->
                         <div class="tab-pane" id="tab_2">
                           <p>Visi Badan Nasional Pengeloal Perbatasan adalah ”Terwujudnya Tata Kelola Perbatasan Negara Yang Efektif Dalam Rangka Perwujudan Kawasan Perbatasan Negara Sebagai Halaman Depan Negara Yang Berdaya-Saing”.
                              Misi Badan Nasional Pengelola Perbatasan yang ditetapkan merupakan peran strategik yang diinginkan dalam mencapai visi diatas yaitu :
                           </p>
                           <ul>
                               <li>Meningkatkan efektifitas dalam penetapan kebijakan dan program pembangunan perbatasan</li>
                               <li>Meningkatkan efektifitas dalam penetapan rencana kebutuhan anggaran pengelolaan perbatasan negara</li>
                               <li>Meningkatkan efektifitas dalam fasilitasi dan koordinasi pelaksanaan pengelolaan perbatasan negara</li>
                               <li>Meningkatkan efektifitas dalam pelaksanaan evaluasi dan pengawasan pengelolaan perbatasan negara; </li>
                          </ul>

                         </div>
                         <!-- /.tab-pane -->
                         <div class="tab-pane" id="tab_3">
                          <p>
                            <b>Penelitian ini dilakukan pada Badan Nasional Pengelola Perbatasan  yang beralamat  di Jalan Kebon Sirih No.13 Jakarta Pusat.</b>
                          </p>
                         </div>
                         <!-- /.tab-pane -->
                       </div>
                       <!-- /.tab-content -->
                     </div>
                     <!-- nav-tabs-custom -->
                   </div>
                   <!-- /.col -->




          </div>
      </section>

    </div>
  </div>

  <script>
      $(function () {
        $('#table-home tfoot th').each( function () {
          var title = $(this).text();
          $(this).html( '<input style="width:100%;" type="text" placeholder="Search '+title+'" />' );
        });

        var table = $('#table-home').DataTable({
          responsive: true,
          stateSave: true,
          "paging": true,
          "lengthChange": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
          "order": [[ 0, "desc" ]],
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          dom: 'lrtipB',
          buttons: [
                  'copy', 'csv', 'excel', 'pdf', 'print'
          ]
        });
        // Apply the search
        table.columns().every( function () {
          var that = this;
          $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
              that
              .search( this.value )
              .draw();
            }
          });
        });

        // for datetimepicker


      });
    </script>
<body>
@endsection
