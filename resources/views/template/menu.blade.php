<!-- Navbar Right Menu -->
<div class="navbar-custom-menu">
  <ul class="nav navbar-nav">
    <!-- Messages: style can be found in dropdown.less-->
    <!-- test edit saja -->
    <!-- User Account: style can be found in dropdown.less -->
    <li class="dropdown user user-menu">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        @if(Auth::user()->photo != '0')
          <img src="{{url('adminlte/dist/img/avatar5.png')}}" class="user-image" alt="User Image">
        @else
          <img src="{{url('adminlte/dist/img/avatar5.png')}}" class="user-image" alt="User Image">
        @endif

        <span class="hidden-xs">{{Auth::user()->email}}</span>
      </a>
      <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
          @if(Auth::user()->photo != '0')
            <img src="{{url('adminlte/dist/img/avatar5.png')}}" class="img-sircle" alt="User Image">
          @else
            <img src="{{url('adminlte/dist/img/avatar5.png')}}" class="img-sircle" alt="User Image">
          @endif
          <p>
          {{Auth::user()->email}}
            <small>Member since {{date_format(Auth::user()->created_at, "M Y")}}</small>
          </p>
        </li>
        <!-- Menu Body -->
        <!-- Menu Footer-->
        <li class="user-footer">
          <div class="pull-left">
            <a href="#" class="btn btn-default btn-flat">Profile</a>
          </div>
          <div class="pull-right">
            <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
          </div>
        </li>
      </ul>
    </li>

  </ul>
</div>
