<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">

        @if(Auth::user()->photo != '0')
          <img src="{{url('adminlte/dist/img/avatar5.png')}}" class="user-image" alt="User Image">
        @else
          <img src="{{url('adminlte/dist/img/avatar5.png')}}" class="user-image" alt="User Image">
        @endif

      </div>
      <div class="pull-left info">
        <p>{{Auth::user()->name}}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MENU</li>
      <li class="">
        <a href="{{url('/home')}}">
          <i class="fa fa-home"></i> <span>Home</span>
        </a>
      </li>
      @if(Auth::user()->name == 'admin')
      <li class="">
        <a href="{{url('/data_pegawai')}}">
          <i class="fa fa-users"></i> <span>Data Pegawai</span>
        </a>
      </li>
      <li class="">
        <a href="{{url('/no_surat')}}">
          <i class="fa fa-sticky-note"></i> <span>Data No. Surat</span>
        </a>
      </li>
      <li class="">
        <a href="{{url('/no_spt')}}">
          <i class="fa fa-sticky-note"></i> <span>Data No. SPT</span>
        </a>
      </li>
      <li class="">
        <a href="{{url('/rincian_biaya')}}">
          <i class="fa fa-dollar"></i> <span>Data Rincian Biaya</span>
        </a>
      </li>
      <li class="">
        <a href="{{url('/laporan_sppd')}}">
          <i class="fa fa-book"></i> <span>Laporan SPPD</span>
        </a>
      </li>
      <li class="">
        <a href="{{url('/galeri')}}">
          <i class="fa fa-image"></i> <span>Galeri</span>
        </a>
      </li>
        @else

        <li class="">
          <a href="{{url('/#')}}">
            <i class="fa fa-dollar"></i> <span>Data Rincian Biaya</span>
          </a>
        </li>

        @endif
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
