@extends('template.app')

{{-- set title --}}
@section('title', 'Manage Posting')

{{-- set main content --}}
@section('content')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>BNPP</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>BNPP</b></span>
    </a>

     <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
          @include('template.menu')
      </nav>
    </header>

         @include('template.sidebar')

    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content-header">
        <h1>
          Galeri
        </h1>
        <ol class="breadcrumb">
          <li>Menu</li>
          <li class="active">Galeri</li>
        </ol>
      </section>

      <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="row">
                     <div class="col-md-12">
                        <div class="box box-solid">
                          <div class="box-header with-border">
                            <h3 class="box-title">BNPP</h3>
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                              <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                                <li data-target="#carousel-example-generic" data-slide-to="3" class=""></li>
                                <li data-target="#carousel-example-generic" data-slide-to="4" class=""></li>
                                <li data-target="#carousel-example-generic" data-slide-to="5" class=""></li>
                                <li data-target="#carousel-example-generic" data-slide-to="6" class=""></li>
                              </ol>
                              <div class="carousel-inner">
                                <div class="item active">
                                  <img src="{{url('upload/gambar1.jpeg')}}" alt="First slide">

                                  <div class="carousel-caption">
                                    Rapat 1
                                  </div>
                                </div>
                                <div class="item">
                                  <img src="{{url('upload/gambar2.jpeg')}}" alt="Second slide">

                                  <div class="carousel-caption">
                                    Rapat 2
                                  </div>
                                </div>
                                <div class="item">
                                  <img src="{{url('upload/gambar3.jpeg')}}" alt="Third slide">

                                  <div class="carousel-caption">
                                    Rapat 3
                                  </div>
                                </div>
                                <div class="item">
                                  <img src="{{url('upload/gambar4.jpeg')}}" alt="Four slide">

                                  <div class="carousel-caption">
                                    Rapat 4
                                  </div>
                                </div>
                                <div class="item">
                                  <img src="{{url('upload/gambar5.jpeg')}}" alt="Five slide">

                                  <div class="carousel-caption">
                                    Rapat 5
                                  </div>
                                </div>
                                <div class="item">
                                  <img src="{{url('upload/gambar6.jpeg')}}" alt="Six slide">

                                  <div class="carousel-caption">
                                    Rapat 6
                                  </div>
                                </div>
                                <div class="item">
                                  <img src="{{url('upload/gambar7.jpeg')}}" alt="Seven slide">

                                  <div class="carousel-caption">
                                    Rapat 7
                                  </div>
                                </div>

                              </div>
                              <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="fa fa-angle-left"></span>
                              </a>
                              <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="fa fa-angle-right"></span>
                              </a>
                            </div>
                          </div>
                          <!-- /.box-body -->
                        </div>
          <!-- /.box -->
        </div>
                     </div>
              </div>
      </section>

    </div>
  </div>

  <script>
      $(function () {
        $('#table-home tfoot th').each( function () {
          var title = $(this).text();
          $(this).html( '<input style="width:100%;" type="text" placeholder="Search '+title+'" />' );
        });

        var table = $('#table-home').DataTable({
          responsive: true,
          stateSave: true,
          "paging": true,
          "lengthChange": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
          "order": [[ 0, "desc" ]],
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          dom: 'lrtipB',
          buttons: [
                  'copy', 'csv', 'excel', 'pdf', 'print'
          ]
        });
        // Apply the search
        table.columns().every( function () {
          var that = this;
          $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
              that
              .search( this.value )
              .draw();
            }
          });
        });

        // for datetimepicker


      });
    </script>
<body>
@endsection
