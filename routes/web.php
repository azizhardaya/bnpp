<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/galeri', 'GaleriController@index')->name('galeri');
Route::get('/', 'HomeController@index')->name('home');

//DataPegawai
Route::get('/data_pegawai', 'DataPegawaiController@index')->name('data_pegawai');
Route::get('/data_pegawai/create', 'DataPegawaiController@create')->name('data_pegawai/create');
Route::post('/data_pegawai/do_create', 'DataPegawaiController@do_create')->name('data_pegawai/do_create');
Route::get('/data_pegawai/edit/{id}', 'DataPegawaiController@edit')->name('data_pegawai/edit');
Route::put('/data_pegawai/do_edit/{id}', 'DataPegawaiController@do_edit')->name('data_pegawai/do_edit');
Route::get('/data_pegawai/view/{id}', 'DataPegawaiController@view')->name('data_pegawai/view');
Route::get('/data_pegawai/delete/{id}', 'DataPegawaiController@delete')->name('data_pegawai/delete');

//NoSurat
Route::get('/no_surat', 'NoSuratController@index')->name('no_surat');
Route::get('/no_surat/create', 'NoSuratController@create')->name('no_surat/create');
Route::post('/no_surat/do_create', 'NoSuratController@do_create')->name('no_surat/do_create');
Route::get('/no_surat/edit/{id}', 'NoSuratController@edit')->name('no_surat/edit');
Route::put('/no_surat/do_edit/{id}', 'NoSuratController@do_edit')->name('no_surat/do_edit');
Route::get('/no_surat/view/{id}', 'NoSuratController@view')->name('no_surat/view');
Route::get('/no_surat/delete/{id}', 'NoSuratController@delete')->name('no_surat/delete');

//NoSPT
Route::get('/no_spt', 'NoSPTController@index')->name('no_spt');
Route::get('/no_spt/create', 'NoSPTController@create')->name('no_spt/create');
Route::post('/no_spt/do_create', 'NoSPTController@do_create')->name('no_spt/do_create');
Route::get('/no_spt/edit/{id}', 'NoSPTController@edit')->name('no_spt/edit');
Route::put('/no_spt/do_edit/{id}', 'NoSPTController@do_edit')->name('no_spt/do_edit');
Route::get('/no_spt/view/{id}', 'NoSPTController@view')->name('no_spt/view');
Route::get('/no_spt/delete/{id}', 'NoSPTController@delete')->name('no_spt/delete');

//Rincian Biaya
Route::get('/rincian_biaya', 'RincianBiayaController@index')->name('rincian_biaya');
Route::get('/rincian_biaya/create', 'RincianBiayaController@create')->name('rincian_biaya/create');
Route::post('/rincian_biaya/do_create', 'RincianBiayaController@do_create')->name('rincian_biaya/do_create');
Route::get('/rincian_biaya/edit/{id}', 'RincianBiayaController@edit')->name('rincian_biaya/edit');
Route::put('/rincian_biaya/do_edit/{id}', 'RincianBiayaController@do_edit')->name('rincian_biaya/do_edit');
Route::get('/rincian_biaya/view/{id}', 'RincianBiayaController@view')->name('rincian_biaya/view');
Route::get('/rincian_biaya/delete/{id}', 'RincianBiayaController@delete')->name('rincian_biaya/delete');

//Laporan SPPD
Route::get('/laporan_sppd', 'LaporanSPPDController@index')->name('laporan_sppd');
Route::get('/laporan_sppd/create', 'LaporanSPPDController@create')->name('laporan_sppd/create');
Route::post('/laporan_sppd/do_create', 'LaporanSPPDController@do_create')->name('laporan_sppd/do_create');
Route::get('/laporan_sppd/edit/{id}', 'LaporanSPPDController@edit')->name('laporan_sppd/edit');
Route::put('/laporan_sppd/do_edit/{id}', 'LaporanSPPDController@do_edit')->name('laporan_sppd/do_edit');
Route::get('/laporan_sppd/view/{id}', 'LaporanSPPDController@view')->name('laporan_sppd/view');
Route::get('/laporan_sppd/delete/{id}', 'LaporanSPPDController@delete')->name('laporan_sppd/delete');
