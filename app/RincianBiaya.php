<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RincianBiaya extends Model
{
  public $timestamps = false; // tidak menggunakan field create_at dan update_at
  public $table = 'rincian_biaya'; // ini untuk menyambungkan ke table

  protected $guarded = ['id']; // untuk field semua yang ditulis oleh system
}
