<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanSPPD extends Model
{
  public $timestamps = false; // tidak menggunakan field create_at dan update_at
  public $table = 'laporan_sppd'; // ini untuk menyambungkan ke table

  protected $guarded = ['id']; // untuk field semua yang ditulis oleh system
}
