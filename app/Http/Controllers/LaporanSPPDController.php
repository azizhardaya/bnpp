<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB; //untuk menggunakan query builder
use App\LaporanSPPD; // untuk menyambungkan ke models
use Auth;

class LaporanSPPDController extends Controller
{
  /**
 * Create a new controller instance.
 *
 * @return void
 */
public function __construct()
{
    $this->middleware('auth');
}

/**
 * Show the application dashboard.
 *
 * @return \Illuminate\Http\Response
 */
  public function index()
  {
      $laporan_sppd = DB::table('laporan_sppd')->paginate(20);
      return view('laporan_sppd/index', ['laporan_sppd' => $laporan_sppd]);
  }

  public function create()
  {
    $nip = DB::table('data_pegawai')->get();
    return view('laporan_sppd/create', ['nip' => $nip]);
  }

  public function do_create(Request $request)
  {
    $data = new LaporanSPPD;
    $data->nip = $request->nip;
    $data->nama_pegawai = $request->nama_pegawai;
    $data->no_sppd = $request->no_sppd;
    $data->kegiatan = $request->kegiatan;
    $data->t_berangkat = $request->t_berangkat;
    $data->t_tujuan = $request->t_tujuan;
    $data->save();

    return redirect(url('laporan_sppd'))->with('status', 'Success');
  }
  public function edit($id)
  {
    $laporan_sppd = LaporanSPPD::find($id);
    return view('laporan_sppd/edit', ['laporan_sppd' => $laporan_sppd]);
  }
  public function do_edit(Request $request, $id)
  {
    $data = LaporanSPPD::find($id);
    $data->nip = $request->nip;
    $data->nama_pegawai = $request->nama_pegawai;
    $data->no_sppd = $request->no_sppd;
    $data->kegiatan = $request->kegiatan;
    $data->t_berangkat = $request->t_berangkat;
    $data->t_tujuan = $request->t_tujuan;
    $data->save();

    return redirect(url('laporan_sppd'))->with('status', 'Success');
  }
  public function view($id)
  {
      //
      $laporan_sppd = LaporanSPPD::find($id);
      return view('laporan_sppd/view', ['laporan_sppd' => $laporan_sppd]);
  }
  public function delete($id)
  {
      //
      LaporanSPPD::destroy($id);
      return redirect(url('laporan_sppd'))->with('status', 'Success');
  }
}
