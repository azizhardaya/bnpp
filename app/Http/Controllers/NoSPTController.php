<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB; //untuk menggunakan query builder
use App\NoSPT; // untuk menyambungkan ke models
use Auth;

class NoSPTController extends Controller
{
  /**
 * Create a new controller instance.
 *
 * @return void
 */
public function __construct()
{
    $this->middleware('auth');
}

/**
 * Show the application dashboard.
 *
 * @return \Illuminate\Http\Response
 */
  public function index()
  {
      $no_spt = DB::table('no_spt')->paginate(20);
      return view('no_spt/index', ['no_spt' => $no_spt]);
  }

  public function create()
  {
    $nip = DB::table('data_pegawai')->get();
    return view('no_spt/create', ['nip' => $nip]);
  }

  public function do_create(Request $request)
  {
    $data = new NoSPT;
    $data->nip = $request->nip;
    $data->nama_pegawai = $request->nama_pegawai;
    $data->no_spt = $request->no_spt;
    $data->golongan = $request->golongan;
    $data->kegiatan = $request->kegiatan;
    $data->rincian_biaya = $request->rincian_biaya;
    $data->save();

    return redirect(url('no_spt'))->with('status', 'Success');
  }
  public function edit($id)
  {
    $no_spt = NoSPT::find($id);
    return view('no_spt/edit', ['no_spt' => $no_spt]);
  }
  public function do_edit(Request $request, $id)
  {
    $data = NoSPT::find($id);
    $data->nip = $request->nip;
    $data->nama_pegawai = $request->nama_pegawai;
    $data->no_spt = $request->no_spt;
    $data->golongan = $request->golongan;
    $data->kegiatan = $request->kegiatan;
    $data->rincian_biaya = $request->rincian_biaya;
    $data->save();

    return redirect(url('no_spt'))->with('status', 'Success');
  }
  public function view($id)
  {
      //
      $no_spt = NoSPT::find($id);
      return view('no_spt/view', ['no_spt' => $no_spt]);
  }
  public function delete($id)
  {
      //
      NoSPT::destroy($id);
      return redirect(url('no_spt'))->with('status', 'Success');
  }
}
