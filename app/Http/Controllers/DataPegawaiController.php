<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB; //untuk menggunakan query builder
use App\DataPegawai; // untuk menyambungkan ke models
use Auth;

class DataPegawaiController extends Controller
{
        /**
       * Create a new controller instance.
       *
       * @return void
       */
      public function __construct()
      {
          $this->middleware('auth');
      }

      /**
       * Show the application dashboard.
       *
       * @return \Illuminate\Http\Response
       */
        public function index()
        {
            $data_pegawai = DB::table('data_pegawai')->paginate(20);
            return view('data_pegawai/index', ['data_pegawai' => $data_pegawai]);
        }

        public function create()
        {
          return view('data_pegawai/create');
        }

        public function do_create(Request $request)
        {
          $data = new DataPegawai;
          $data->nip = $request->nip;
          $data->nama_pegawai = $request->nama_pegawai;
          $data->alamat = $request->alamat;
          $data->golongan = $request->golongan;
          $data->jabatan = $request->jabatan;
          $data->no_tlp = $request->no_tlp;
          $data->email = $request->email;
          $data->save();

          return redirect(url('data_pegawai'))->with('status', 'Success');
        }
        public function edit($id)
        {
          $data_pegawai = DataPegawai::find($id);
          return view('data_pegawai/edit', ['data_pegawai' => $data_pegawai]);
        }
        public function do_edit(Request $request, $id)
        {
          $data = DataPegawai::find($id);
          $data->nip = $request->nip;
          $data->nama_pegawai = $request->nama_pegawai;
          $data->alamat = $request->alamat;
          $data->golongan = $request->golongan;
          $data->jabatan = $request->jabatan;
          $data->no_tlp = $request->no_tlp;
          $data->email = $request->email;
          $data->save();

          return redirect(url('data_pegawai'))->with('status', 'Success');
        }
        public function view($id)
        {
            //
            $data_pegawai = DataPegawai::find($id);
            return view('data_pegawai/view', ['data_pegawai' => $data_pegawai]);
        }
        public function delete($id)
        {
            //
            DataPegawai::destroy($id);
            return redirect(url('data_pegawai'))->with('status', 'Success');
        }
}
