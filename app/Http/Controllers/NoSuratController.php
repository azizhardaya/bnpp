<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB; //untuk menggunakan query builder
use App\NoSurat; // untuk menyambungkan ke models
use Auth;

class NoSuratController extends Controller
{
  /**
 * Create a new controller instance.
 *
 * @return void
 */
public function __construct()
{
    $this->middleware('auth');
}

/**
 * Show the application dashboard.
 *
 * @return \Illuminate\Http\Response
 */
  public function index()
  {
      $no_surat = DB::table('no_surat')->paginate(20);
      return view('no_surat/index', ['no_surat' => $no_surat]);
  }

  public function create()
  {
    $nip = DB::table('data_pegawai')->get();
    return view('no_surat/create', ['nip' => $nip]);
  }

  public function do_create(Request $request)
  {
    $data = new NoSurat;
    $data->nip = $request->nip;
    $data->no_surat = $request->no_surat;
    $data->isi_surat = $request->isi_surat;
    $data->save();

    return redirect(url('no_surat'))->with('status', 'Success');
  }
  public function edit($id)
  {
    $no_surat = NoSurat::find($id);
    return view('no_surat/edit', ['no_surat' => $no_surat]);
  }
  public function do_edit(Request $request, $id)
  {
    $data = NoSurat::find($id);
    $data->nip = $request->nip;
    $data->no_surat = $request->no_surat;
    $data->isi_surat = $request->isi_surat;
    $data->save();

    return redirect(url('no_surat'))->with('status', 'Success');
  }
  public function view($id)
  {
      //
      $no_surat = NoSurat::find($id);
      return view('no_surat/view', ['no_surat' => $no_surat]);
  }
  public function delete($id)
  {
      //
      NoSurat::destroy($id);
      return redirect(url('no_surat'))->with('status', 'Success');
  }
}
