<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB; //untuk menggunakan query builder
use App\RincianBiaya; // untuk menyambungkan ke models
use Auth;

class RincianBiayaController extends Controller
{
  /**
 * Create a new controller instance.
 *
 * @return void
 */
public function __construct()
{
    $this->middleware('auth');
}

/**
 * Show the application dashboard.
 *
 * @return \Illuminate\Http\Response
 */
  public function index()
  {
      $rincian_biaya = DB::table('rincian_biaya')->paginate(20);
      return view('rincian_biaya/index', ['rincian_biaya' => $rincian_biaya]);
  }

  public function create()
  {
    $nip = DB::table('data_pegawai')->get();
    return view('rincian_biaya/create', ['nip' => $nip]);
  }

  public function do_create(Request $request)
  {
    $data = new RincianBiaya;
    $data->nip = $request->nip;
    $data->nama_pegawai = $request->nama_pegawai;
    $data->harian= $request->harian;
    $data->transport = $request->transport;
    $data->uang_hotel = $request->uang_hotel;
    $data->jabatan = $request->jabatan;
    $data->kegiatan = $request->kegiatan;
    $data->save();

    return redirect(url('rincian_biaya'))->with('status', 'Success');
  }
  public function edit($id)
  {
    $rincian_biaya = RincianBiaya::find($id);
    return view('rincian_biaya/edit', ['rincian_biaya' => $rincian_biaya]);
  }
  public function do_edit(Request $request, $id)
  {
    $data = RincianBiaya::find($id);
    $data->nip = $request->nip;
    $data->nama_pegawai = $request->nama_pegawai;
    $data->harian= $request->harian;
    $data->transport = $request->transport;
    $data->uang_hotel = $request->uang_hotel;
    $data->jabatan = $request->jabatan;
    $data->kegiatan = $request->kegiatan;
    $data->save();

    return redirect(url('rincian_biaya'))->with('status', 'Success');
  }
  public function view($id)
  {
      //
      $rincian_biaya = RincianBiaya::find($id);
      return view('rincian_biaya/view', ['rincian_biaya' => $rincian_biaya]);
  }
  public function delete($id)
  {
      //
      RincianBiaya::destroy($id);
      return redirect(url('rincian_biaya'))->with('status', 'Success');
  }
}
