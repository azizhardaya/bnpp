<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataPegawai extends Model
{
  public $timestamps = false; // tidak menggunakan field create_at dan update_at
  public $table = 'data_pegawai'; // ini untuk menyambungkan ke table

  protected $guarded = ['id']; // untuk field semua yang ditulis oleh system
}
